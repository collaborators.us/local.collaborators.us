# Local collaborators.us development environment

Execute the setup script (`setup.cmd` on Windows and `setup.sh` on Mac or Linux),
then answer the prompts.

Prerequisites:
 - Docker: https://docs.docker.com/install/#supported-platforms

