@ECHO OFF
SETLOCAL

docker node ls 2>&1 > nul
IF ERRORLEVEL 1 (
    echo "Initializing Docker swarm mode..."
    docker swarm init > nul
)

docker service ls | find /i "traefik" >2>&1 > nul
IF ERRORLEVEL 1 (
    echo Deploying Traefik to your Docker swarm...
    docker stack deploy -c traefik/docker-compose.yml traefik > nul
)

REM TODO: hosts file
