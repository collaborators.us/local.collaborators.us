#!/bin/sh

if ! $(docker node ls > /dev/null 2>&1); then
    echo "Initializing Docker swarm mode..."
    docker swarm init
fi

if ! $(docker service ls | grep -i traefik > /dev/null 2>&1); then
    echo "Deploying Traefik to your Docker swarm..."
    docker stack deploy -c traefik/docker-compose.yml traefik
fi

# TODO: hosts file
